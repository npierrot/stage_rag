import PyPDF2
from langchain_community.embeddings import HuggingFaceEmbeddings
from langchain_community.vectorstores import Annoy
from langchain_text_splitters import RecursiveCharacterTextSplitter
from langchain.chains.combine_documents import create_stuff_documents_chain
from langchain_core.prompts import ChatPromptTemplate
from langchain.chains import create_retrieval_chain
from langchain_community.llms.huggingface_pipeline import HuggingFacePipeline
from langchain_community.document_loaders import TextLoader

pdf_reader = PyPDF2.PdfReader(
    'annexe-2-licence-professionnelle-bachelor-universitaire-de-technologie-informatique-29016.pdf')
extracted_text = ''.join(page.extract_text() for page in pdf_reader.pages)

with open("pdf_en_txt.txt", "w", encoding="utf-8") as fichier:
    fichier.write(extracted_text)
docs = TextLoader("./pdf_en_txt.txt", encoding="utf-8")
documents = docs.load()

text_splitter = RecursiveCharacterTextSplitter(chunk_size=150, chunk_overlap=0)
documents_split = text_splitter.split_documents(documents)

prompt = ChatPromptTemplate.from_template("""Réponds à la question suivante en te uniquement sur le contexte
fourni:

{context}

Question: {input}""")

llm = HuggingFacePipeline.from_model_id(
    model_id="croissantllm/CroissantLLMChat-v0.1",
    task="text-generation",
    device_map="cuda",
    pipeline_kwargs={"max_new_tokens": 50},
)

# embeddings = HuggingFaceEmbeddings(model_name="all-mpnet-base-v2")
embeddings = HuggingFaceEmbeddings(model_name="all-MiniLM-L6-v2")

vectorstore = Annoy.from_documents(
    documents_split, embedding=embeddings
)
retriever = vectorstore.as_retriever()

document_chain = create_stuff_documents_chain(llm, prompt)
retrieval_chain = create_retrieval_chain(retriever, document_chain)

print(retrieval_chain.invoke({"input": "Est-ce que des stages sont obligatoires pendant la formation ?"})["answer"])

# i = 1
#
# # On ouvre le fichier des questions
# with open('Liste_Questions.txt', 'r', encoding='utf-8') as fichier:
#     # on lit chaque ligne du fichier
#     for ligne in fichier:
#         print("ligne " + str(i))
#         # on génère la réponse de la question
#         reponse = retrieval_chain.invoke({"input": ligne})
#         # on ouvre un nouveau fichier pour y écrire les questions, les réponses que le modèle a générées et les
#         # passages du pdf qui ont servi à produire ces réponses
#         with open('Questions_et_Reponses_v4.txt', 'a', encoding='utf-8') as fichier2:
#             # on affiche la question et la réponse
#             fichier2.write("Question " + str(i) + " :\n" + ligne + '\n'
#                            "Réponse " + str(i) + " :\n" + reponse['answer'] + '\n\n')
#             # on affiche le contexte qui a servi à produire la réponse
#             j = 1
#             fichier2.write("/===============\\Contexte : /===============\\\n")
#             for doc in reponse['context']:
#                 fichier2.write("\n=============== Contexte " + str(j) + " : ===============\n" + doc.page_content)
#                 j += 1
#
#             fichier2.write('\n\n########################################################################\n\n')
#             i += 1

