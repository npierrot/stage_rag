import faiss
import numpy as np
from langchain.chains.combine_documents import create_stuff_documents_chain
from langchain.chains import create_retrieval_chain
from langchain_community.document_loaders import TextLoader
from langchain_community.embeddings import HuggingFaceEmbeddings
from langchain_community.llms.huggingface_pipeline import HuggingFacePipeline
from langchain_core.prompts import ChatPromptTemplate
from langchain_text_splitters import RecursiveCharacterTextSplitter

docs = TextLoader("./pdf_en_txt_sans_sommaire.txt", encoding="utf-8")
documents = docs.load()

text_splitter = RecursiveCharacterTextSplitter(chunk_size=200, chunk_overlap=30)
documents_split = text_splitter.split_documents(documents)

prompt = ChatPromptTemplate.from_template("""Réponds à la question suivante en te basant uniquement sur le contexte fourni:

{context}

Question: {input}""")

llm = HuggingFacePipeline.from_model_id(
    model_id="croissantllm/CroissantLLMChat-v0.1",
    task="text-generation",
    device_map="cuda",
    pipeline_kwargs={"max_new_tokens": 50},
)

embeddings = HuggingFaceEmbeddings(model_name="all-MiniLM-L6-v2")  # le meilleur modèle que j'ai trouvé

d = 64
nb = len(documents_split)
nq = 10000  # nb of queries

xb = np.random.random((nb, d)).astype('float32')
xb[:, 0] += np.arange(nb) / 1000.
xq = np.random.random((nq, d)).astype('float32')
xq[:, 0] += np.arange(nq) / 1000.

# Création de l'index FAISS
vectorstore = faiss.IndexFlatL2(d)
print(vectorstore)
vectorstore = vectorstore.add(xq)
print(vectorstore)

# k = 4                          # we want to see 4 nearest neighbors
#
# D, I = vectorstore.search(xq, k)     # actual search
# print(I[:5])                   # neighbors of the 5 first queries
# print(I[-5:])                  # neighbors of the 5 last queries
#
#
# # vectorstore.add(embeddings)
#
#
# def search(query):
#     # Créer un vecteur d'embedding pour la requête
#     query_embedding = embeddings.embed_query(query)
#
#     # Effectuer une recherche dans l'index FAISS pour trouver les vecteurs les plus proches du vecteur de la requête
#     k = 5  # Nombre de résultats à renvoyer
#     _, I = vectorstore.search(np.array([query_embedding]), k)
#
#     # Récupérer les documents correspondants aux vecteurs les plus proches
#     results = []
#     for i in range(k):
#         index = I[0][i]
#         result = documents_split[index]
#         results.append(result)
#
#     return results
#
#
# # Exemple d'utilisation
# query = "Quelle est le Nbre de semaines de stage en BUT 3 ?"
# results = search(query)
# for result in results:
#     print(result.page_content)

#
# # on spécifie ici que la recherche se fait par similarité ou mmr et on ajoute un nombre de "contexte" à retenir
# # mmr semble plus adapté, il permet de récupérer des contextes plus diversifiés (Maximal Marginal Relevance)
# retriever = vectorstore.as_retriever(
#     embedding=embeddings,
#     search_type="mmr",  # autre possibilité : similarity
#     distance_metric="L2",  # L2 pour distance euclidienne et IP pour produit scalaire
#     search_kwargs={"k": 4},  # nombre de contextes à sélectionner
# )
#
# # top_k = 100,
#
# document_chain = create_stuff_documents_chain(llm, prompt)
# retrieval_chain = create_retrieval_chain(retriever, document_chain)
#
# reponse = retrieval_chain.invoke({"input": "Quelle est le Nbre de semaines de stage en BUT 3 ?"})
# j = 1
# for doc in reponse['context']:
#     print("\n=============== Contexte " + str(j) + " : ===============\n" + doc.page_content)
#     j += 1
#
# print(reponse["answer"])
#
# i = 1
#
# # On ouvre le fichier des questions
# with open('Liste_Questions.txt', 'r', encoding='utf-8') as fichier:
#     # on lit chaque ligne du fichier, qui correspondent à une question chacune
#     for ligne in fichier:
#         print("ligne " + str(i))
#         # on génère la réponse de la question
#         reponse = retrieval_chain.invoke({"input": ligne})
#         # on ouvre un nouveau fichier pour y écrire les questions, les réponses que le modèle a générées et les
#         # passages du pdf qui ont servi à produire ces réponses
#         with open('./Comparaison_retriever_1/FLANN_Questions_et_Reponses.txt', 'a', encoding='utf-8') as fichier2:
#             # on affiche la question et la réponse
#             fichier2.write("Question " + str(i) + " :\n" + ligne + '\n'
#                            "Réponse " + str(i) + " :\n" + reponse['answer'] + '\n\n')
#             # on affiche le contexte qui a servi à produire la réponse
#             j = 1
#             fichier2.write("/===============\\ Contextes : /===============\\\n")
#             for doc in reponse['context']:
#                 fichier2.write("\n=============== Contexte " + str(j) + " : ===============\n" + doc.page_content)
#                 j += 1
#
#             fichier2.write('\n\n########################################################################\n\n')
#             i += 1
#
# print("C'est enfin fini !")
