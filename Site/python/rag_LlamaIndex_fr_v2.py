# ne pas oublier de lancer le serveur local "ollama serve" dans un terminal avant de lancer le programme
# temps d'execution : environ 3min

from langchain_community.embeddings import HuggingFaceEmbeddings
from llama_index.core import VectorStoreIndex, Settings, ChatPromptTemplate
from llama_index.core.base.llms.types import ChatMessage, MessageRole
from llama_index.core.indices.vector_store import VectorIndexRetriever
from llama_index.core.query_engine import RetrieverQueryEngine
from llama_index.llms.ollama import Ollama
from llama_index.readers.file import PyMuPDFReader

def load_index():
    # on importe le fichier à utiliser
    file_path = "pdf/annexe-2-licence-professionnelle-bachelor-universitaire-de-technologie-informatique-29016.pdf"  # chemin du fichier
    reader = PyMuPDFReader()  # on charge le lecteur de documents
    documents = reader.load(file_path)  # on transforme le pdf en un document texte

    # modèle utilisé pour générer les embeddings
    # meilleur compromis (temps/qualité de réponse) : intfloat/multilingual-e5-base
    embedding_model = HuggingFaceEmbeddings(model_name="intfloat/multilingual-e5-base")

    # on transforme le pdf en vecteur de données avec les embeddings et on le divise
    index = VectorStoreIndex.from_documents(
        documents,  # documents à utliser
        embed_model=embedding_model,  # modèle d'embeddings à utiliser
        show_progress=True,  # argument qui permet d'afficher la progression de la transformation du pdf en un index
    )

    # définition du llm
    mistral = Ollama(
        model="mistral",  # nom du modèle à utiliser
        request_timeout=6000.0,  # paramètre obligatoire, qui permet d'arrêter le programme si la génération est trop longue
    )

    # définition du retriever et des paramètres nécessaires
    retriever = VectorIndexRetriever(
        index=index,  # index à utiliser pour faire la recherche
        similarity_top_k=4,  # nombre de contextes à récupérer
    )

    # définition de paramètres globaux
    Settings.chunk_size = 150  # taille des contextes (en tokens)
    Settings.chunk_overlap = 10  # superposition entre deux contextes (en tokens)
    Settings.num_output = 10  # taille de la réponse (en tokens)
    Settings.llm = mistral  # définition du llm

    # redéfinition de la template que le modèle va devoir suivre (elle est à la base en anglais)
    chat_text_qa_msgs = [
        ChatMessage(
            role=MessageRole.SYSTEM,
            content=(
                "Vous êtes un système expert de questions-réponses auquel le monde entier fait confiance.\n"
                "Répondez toujours à la requête en utilisant les informations contextuelles fournies"
                "et non vos connaissances préalables.\n"
                "Quelques règles à suivre :\n"
                "1. Ne faites jamais directement référence au contexte donné dans votre réponse.\n"
                "2. Ne faites jamais de déclarations telles que \"D'après le contexte, ...\" ou "
                "\"Les informations contextuelles ...\", \"Dans le contexte, ...\" ou toute autre déclaration de ce type."

            ),
        ),
        ChatMessage(
            role=MessageRole.USER,
            content=(
                "Les informations contextuelles sont ci-dessous.\n"
                "---------------------\n"
                "{context_str}\n"
                "---------------------\n"
                "Compte tenu des informations contextuelles, sans connaissances préalables et sans citez le contexte, répondez à la requête en français.\n"
                "Requête : {query_str}\n"
                "Réponse : "
            ),
        ),
    ]
    text_qa_template = ChatPromptTemplate(chat_text_qa_msgs)  # on transforme le texte de la template en un élément template

    # création du "moteur" du rag
    query_engine = RetrieverQueryEngine(
        retriever=retriever,  # le retriever pour faire la recherche des passages les plus pertinents
    ).from_args(  # si on a des arguments supplémentaires, c'est ici qu'il faut les ajouter
        retriever=retriever,  # on le remet ici parce que c'est un paramètre obligatoire
        text_qa_template=text_qa_template,  # on indique la template à suivre
    )

    return query_engine

def query(query_engine, question):
    return str(query_engine.query(question))
