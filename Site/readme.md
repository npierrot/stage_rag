## La démonstration

Après avoir récupéré la projet, avec `git clone https://gitlab.inria.fr/npierrot/stage_rag.git` ou `git clone git@gitlab.inria.fr:npierrot/stage_rag.git`  

### Installation de l'environnement

Dans ce dossier, ouvrir un terminal et recréer un nouvel environnement virtuel ([voir la documentation ici](https://gitlab.inria.fr/npierrot/stage_rag#cr%C3%A9ation-de-lenvironnement-python)). Puis, suivre les étapes ci dessous.  

#### Installation de la bibliothèque Flask  
`pip install flask`  
`pip install flask_core`  

On peut alors lancer l'application qui va faire le lien entre le site web et le programme python : `python ./app.py`  
Il faut maintenant attendre que le programme ait fini de charger. Un message similaire devrait apparaitre : ![message qui dit que le serveur est en train de tourner](../img/le_serveur_tourne.png)  

Maintenant, on peut lancer le serveur local dans un terminal à part : `ollama serve`  
Attention à bien fermer ollama s'il s'était ouvert sinon une erreur similaire va se produire :  
![image de l'erreur](../img/erreur_ollama_en_cours_dexecution.png)  

C'est tout bon, vous pouvez à ce moment là lancer le site web et poser vos questions.