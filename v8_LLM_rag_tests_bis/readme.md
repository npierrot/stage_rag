## Le RAG  

Après avoir récupéré la projet, avec `git clone https://gitlab.inria.fr/npierrot/stage_rag.git` ou `git clone git@gitlab.inria.fr:npierrot/stage_rag.git`  

### Installation de l'environnement  

Dans ce dossier, ouvrir un terminal et suivre les étapes ci dessous.  

#### Création de l’environnement python  
`python -m venv .env`  

#### Activation de l’environnement  
`.env/Scripts/activate`  

#### Ensuite, dans l’environnement :  
**Installation de LangChain pour les embeddings**  
`pip install langchain`  
`pip install langchain_community`  
`pip install sentence-transformers`  

**Pour Llama_Index :**  
`pip install llama_index`  
`pip install llama_index.llms.ollama`  
`pip install pymupdf`  
`pip install llama-index-embeddings-langchain`  

### Serveur local avec Ollama :  
Télégarcher ollama : https://ollama.com/download  
Télécharcher le modèle llama2 (le lancer va le téléchager) : `ollama run llama2`  
Une fois que la commande est exécutée, on peut quitter llama2.  
Puis, faire de même avec mistral : `ollama run mistral` et quitter quand le modèle est téléchargé.  
*(Ollama va être utilisé pour le rag en anglais et mistral pour le rag en français)*  

Lancer le serveur local dans un terminal à part : `ollama serve`  
Attention à bien fermer ollama s'il s'était ouvert sinon une erreur similaire va se produire :  
![image de l'erreur](../img/erreur_ollama_en_cours_dexecution.png)  

Vous pouvez maintenant retourner dans l'environnement python et executer le programme avec la commande suivante : `python .\rag_LlamaIndex_en.py`  