# ne pas oublier de lancer le serveur local "ollama serve" dans un terminal avant de lancer le programme
# temps d'execution : environ 2min

from langchain_community.embeddings import HuggingFaceEmbeddings
from llama_index.core import VectorStoreIndex, Settings
from llama_index.core.indices.vector_store import VectorIndexRetriever
from llama_index.core.query_engine import RetrieverQueryEngine
from llama_index.llms.ollama import Ollama
from llama_index.readers.file import PyMuPDFReader

# on importe le document pdf et on le transforme en texte
documents = PyMuPDFReader().load("pdf/Eloquent_JavaScript.pdf")

# on définit le modèle utilisé pour les embeddings
embedding_model = HuggingFaceEmbeddings(model_name="BAAI/bge-small-en-v1.5")

# on transforme le pdf en vecteur de données avec les embeddings et on le divise
index = VectorStoreIndex.from_documents(
    documents,  # documents à utiliser
    embed_model=embedding_model,  # modèle d'embeddings à utiliser
    show_progress=True,  # argument qui permet d'afficher la progression de la transformation du pdf en un index
)

# définition du llm
llama = Ollama(
    model="llama2",  # nom du modèle à utiliser
    request_timeout=600.0,  # paramètre obligatoire, qui permet d'arrêter le programme si la génération est trop longue
)

# définition du retriever et des paramètres nécessaires
retriever = VectorIndexRetriever(
    index=index,  # index à utiliser pour faire la recherche
    similarity_top_k=3,  # nombre de contextes à récupérer
)

# définition de paramètres globaux
Settings.chunk_size = 150  # taille des contextes (en tokens)
Settings.chunk_overlap = 10  # superposition entre deux contextes (en tokens)
Settings.num_output = 10  # taille de la réponse (en tokens)
Settings.llm = llama  # définition du llm

# création du "moteur" du rag
query_engine = RetrieverQueryEngine(
    retriever=retriever   # le retriever sert à faire la recherche des passages les plus pertinents
)

# question = "Who is Carla?"  # question à poser
# reponse = query_engine.query(question)  # réponse à la question

# # on affiche la question et la réponse
# print("Question : ", question, "\nRéponse : ", reponse)

# pour pourvoir poser des questions en boucle (c'est le cas de le dire)
intro = "Ask a question :\n-> "
msg = input(intro)
while msg != 'exit':  # on attend "exit" pour sortir de la boucle
    print(query_engine.query(msg))
    msg = input(intro)

# on arrête le programme ici
exit(0)

# intro = "Posez une question :\n-> "  # exemple : Who is Carla?
# msg = input(intro)
# while msg != 'quit':
#     results = retriever.retrieve(msg)
#     i = 1
#     for result in results:
#         print("###########################################\n"
#               "############### Passage " + str(i) + " : ###############\n"
#               "###########################################")
#         print(result.node.text)
#         i += 1
#     print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
#     print(query_engine.query(msg))
#     msg = input(intro)


# partie pour sauvegarder les questions/réposnes/contextes dans un fichier txt
i = 1
# On ouvre le fichier des questions
with open('questions_reponses_Eloquent/questions_en.txt', 'r', encoding='utf-8') as fichier:
    # on lit chaque ligne du fichier, qui correspondent à une question chacune
    for ligne in fichier:
        print("ligne " + str(i))

        # on enlève le \n qui se trouve à la fin de la question !
        ligne = ligne.replace('\n', '')  # on remplace les \n par rien du tout

        # on génère la réponse de la question
        rep = query_engine.query(ligne)
        # on ouvre un nouveau fichier pour y écrire les questions, les réponses que le modèle a générées et les
        # passages du pdf qui ont servi à produire ces réponses
        with open('./questions_reponses_Eloquent/questions_reponses1.txt', 'a', encoding='utf-8') as fichier2:
            results = retriever.retrieve(ligne)
            j = 1
            fichier2.write("********** " + ligne + " **********\n\n")
            for result in results:
                fichier2.write("\n###########################################\n"
                               "############### Passage " + str(j) + " : ###############\n"
                               "###########################################\n\n")
                fichier2.write(result.node.text)
                fichier2.write("\n")
                j += 1
            # on affiche la question et la réponse
            fichier2.write("\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
                           "Question " + str(i) + " :\n" + ligne + '\n\n'
                           "Réponse " + str(i) + " :\n" + str(rep) + '\n\n')
            # # on affiche le contexte qui a servi à produire la réponse
            # j = 1
            # fichier2.write("/===============\\ Contextes : /===============\\\n")
            # for doc in reponse['context']:
            #     fichier2.write("\n=============== Contexte " + str(j) + " : ===============\n" + doc.page_content)
            #     j += 1

            fichier2.write('\n\n########################################################################\n\n')
            i += 1

print("C'est enfin fini !")
