********** Who took the photos in the document? **********


###########################################
############### Passage 1 : ###############
###########################################

Do you have any comments or views about this 
e-Newsletter? Please send it to e-newsletter@unic-jakarta.org 
 
Published by:  
United Nations Information Centre - Jakarta in Collaboration with the United Nations Communication Group 
This e-Newsletter is for information only

###########################################
############### Passage 2 : ###############
###########################################

Prabowo, 
 
Mr.Marco, Prof. Adrianus Mooy/former Execu
 
tive Secretary of UNES CAP, Mr. H.S. Dillon and UN
 
FPA Country Director/ Resident Representative a.i. 
 
Mr.Zahidul Huque). Photo credit: UNIC Ja-
 
karta  
 
 
 
 
 
 
 
 
 
UNIC internships (L-R: R. Adya, Regina, Petty) 
and Mr. Veriyatna from UNDP in front to the 
Papua Conference Room. Photo credit: UNIC 
Jakarta  
 
 
 
 
 
 
 
 
 
Former Executive Secretary of UNESCAP 
(Center-glasses) with Mr. Marco (left with tie) and 
Dr.

###########################################
############### Passage 3 : ###############
###########################################

Marco (left with tie) and 
Dr. Prabowo grey suite) along with students from 
Universitas Kristen Indonesia (UKI). Photo credit: 
UNIC Jakarta  
Some pictures from the Launch

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Question 1 :
Who took the photos in the document?

Réponse 1 :
Based on the provided context information, it appears that the photos in the e-Newsletter were taken by United Nations Information Centre - Jakarta staff members. The names of the photographers mentioned in the context are:

* R. Adya
* Regina
* Petty
* Mr. Veriyatna (from UNDP)
* Former Executive Secretary of UNESCAP (glasses)
* Marco (left with tie) and Dr. Prabowo (grey suit) along with students from Universitas Kristen Indonesia (UKI)

Therefore, the answer to the query is: United Nations Information Centre - Jakarta staff members.



########################################################################

********** Give me 3 names of the authors of the photos in the document. **********


###########################################
############### Passage 1 : ###############
###########################################

Do you have any comments or views about this 
e-Newsletter? Please send it to e-newsletter@unic-jakarta.org 
 
Published by:  
United Nations Information Centre - Jakarta in Collaboration with the United Nations Communication Group 
This e-Newsletter is for information only

###########################################
############### Passage 2 : ###############
###########################################

Marco (left with tie) and 
Dr. Prabowo grey suite) along with students from 
Universitas Kristen Indonesia (UKI). Photo credit: 
UNIC Jakarta  
Some pictures from the Launch

###########################################
############### Passage 3 : ###############
###########################################

3 
 
Detail list of Emergency supply provided by Crisis Centre, 
MoH. (www.who.int) 
 
 
 
 
 
 
 
 
 
 
Page 3 
 Rapid health assessment conducted by Cri-
 sis Centre & EHA-WHO Team. Photo 
 credit:  WHO 
 

Rapid Assessment Team formed 
and deployed 

Injured and ill cases referred to 
District Hospitals. 

Establish 7 Health Post in the 
affected areas. 

Mobile clinics activated to provide 
mobile health care services. 

MoH deploy 10 teams of psychi-
atrics to help the survivors experienc-
ing serious psychological disorders. 

100 teams will be deployed by 
MoH to conduct fogging.

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Question 2 :
Give me 3 names of the authors of the photos in the document.

Réponse 2 :
Based on the provided context, I cannot directly reference the names of the authors of the photos in the document. However, I can provide you with a list of possible candidates who could be the authors of the photos based on their roles and responsibilities as mentioned in the context information:

1. Marco (left with tie) - As the photo credit indicates, Marco is one of the individuals in the photo, which suggests that he may have taken the photo himself or have some involvement in its creation.
2. Dr. Prabowo - As the photo credit also indicates, Dr. Prabowo is another individual in the photo, and he could have potentially taken the photo or been involved in its creation.
3. Unknown photographer (credited as "UNIC Jakarta") - The third photo credit reads "UNIC Jakarta," which suggests that the photos were taken by an unspecified photographer who works for the United Nations Information Centre in Jakarta.

Therefore, based on the context information provided, the three possible authors of the photos are:

1. Marco
2. Dr. Prabowo
3. Unknown photographer (credited as "UNIC Jakarta")



########################################################################

********** What can you tell me about the dam that collapsed? **********


###########################################
############### Passage 1 : ###############
###########################################

2 
 
Inside Story Headline 
Collapsed Dam of Situ Gintung, Tangerang, Banten Province 
On Friday morning, 27th 
of March 2009, at 2:00 am, 
Situ Gintung dam burst out 
in Cirendeu, Ciputat, Tan-
gerang, Banten Province, 
triggered by the heavy tor-
rential rains. 
10 foot-high wave of wa-
ter flushed into a crowded 
neighborhood, submerging 
400 houses. 98 died, 190 injured, 5 missing, and 171 persons be-
came internally displaced. 2 inpatients in Fatmawati Hospital, and 
139 outpatients.

###########################################
############### Passage 2 : ###############
###########################################


The collapse of the Situ Gintung 
was caused by the inability to ac-
commodate more water on account 
of heavy rains which poured down 
in some districts in Banten and 
West Java provinces all night long 
on Thursday, 26th March 2009. 

The dam was first overflown and 
then burst out, because the founda-
tion was not made of concrete. 

Cracks had already been dis-
covered at the bottom of the em-
bankment last February 2008. The 
administration had since prepared 
evacuation routes in case of flood-
ing and also held a flood evacuation 
simulation exercise last year.

###########################################
############### Passage 3 : ###############
###########################################

1 
 
Launching on The Economic and Social Survey of 
Asia and the Pacific 2009   
 
 
 
 
 
UNIC Jakarta 
Newsletter  
Headline news: 
Launching on the Eco-
nomic and Social Sur-
vey of Asia and The 
Pacific 2009 
1 
Top issue: 
Collapsed Dam of Situ 
Gintung, Tangerang, 
Banten Province 
2 
Collapsed Dam of Situ 
Gintung, Tangerang, 
Banten Province 
(Continued) 
3 
Stories from UN 
agency: 
Indonesia Plans Moni-
toring System on the 
Impact of the Eco-
nomic Crisis on the 
Poor and Vulnerable 
 
4 
Medical Record Man-
agement Training.

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Question 3 :
What can you tell me about the dam that collapsed?

Réponse 3 :
The dam that collapsed was the Situ Gintung dam in Tangerang, Banten Province. The collapse occurred on March 27, 2009, at around 2:00 am, due to heavy torrential rains that poured down in some districts in Banten and West Java provinces all night long. The dam was found to have cracks at the bottom of its embankment last February 2008, but the administration had not taken adequate measures to address the issue. As a result, the dam overflowed and then burst, causing a wave of water to submerge a crowded neighborhood and resulting in significant loss of life and property.



########################################################################

********** Who were the speakers at the launch of the Economic and Social Survey for Asia and the Pacific? **********


###########################################
############### Passage 1 : ###############
###########################################

The Economic and Social Survey of 
Asia and the Pacific 2009 analyses these 
threats and outlines ways in which econo-
mies in the region can move forward in uni-
son from crisis resilience to crisis resistance. 
Download the complete Survey 
http://www.unescap.org/survey2009/ 
Did you know : 
Trygve Halvdan Lie was born 
on 16 July 1896, in Oslo, Nor-
way, the son of Martin and 
Hulda Arnesen. On 1 February 
1946, Mr. Lie was elected as the 
First Secretary-General of the 
Uni ted  Nation s.  Det ail s, 
http://www.un.org/sg/lie.shtml

###########################################
############### Passage 2 : ###############
###########################################

1 
 
Launching on The Economic and Social Survey of 
Asia and the Pacific 2009   
 
 
 
 
 
UNIC Jakarta 
Newsletter  
Headline news: 
Launching on the Eco-
nomic and Social Sur-
vey of Asia and The 
Pacific 2009 
1 
Top issue: 
Collapsed Dam of Situ 
Gintung, Tangerang, 
Banten Province 
2 
Collapsed Dam of Situ 
Gintung, Tangerang, 
Banten Province 
(Continued) 
3 
Stories from UN 
agency: 
Indonesia Plans Moni-
toring System on the 
Impact of the Eco-
nomic Crisis on the 
Poor and Vulnerable 
 
4 
Medical Record Man-
agement Training.

###########################################
############### Passage 3 : ###############
###########################################

The ceremony was attended by 
H.E. Aurora Bernaldez Dicenta, Ambassador of Spain 
to Indonesia and El-Mostafa Benlamlih, UN Resident 
Coordinator and UNDP Resident Representative to 
Indonesia. (www.undp.or.id) 
Reports From UNESCAP 
Download PDF Version of Economic and 
Social Survey of Asia and the Pacific 2009 
Wish to know more other 
UNICs/UNIS around the 
w o r l d ?  P l e a s e  v i s i t 
www.unic.un.org or click 
on the image if you are 
viewing it through PDF 
Wish to know more other 
UNICs/UNIS around the world?

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Question 4 :
Who were the speakers at the launch of the Economic and Social Survey for Asia and the Pacific?

Réponse 4 :
Based on the provided context information, the speakers at the launch of the Economic and Social Survey of Asia and the Pacific in 2009 were H.E. Aurora Bernaldez Dicenta, Ambassador of Spain to Indonesia and El-Mostafa Benlamlih, UN Resident Coordinator and UNDP Resident Representative to Indonesia.



########################################################################

********** How did the Ministry of Health and WHO respond to the Situ Gintung dam collapse? **********


###########################################
############### Passage 1 : ###############
###########################################

2 inpatients in Fatmawati Hospital, and 
139 outpatients. 
MOH, Crisis Center, Provincial Health Office and Tangerang Dis-
trict Health Office are in close collaboration with WHO Indonesia, 
provided integrated Emergency Health Services according to Stan-
dard Operating Procedures.MOH had been established command 
post and temporary evacuation site at Muhammadiyah University of 
Jakarta and STIE Ahmad Dahlan. (www.who.int) 
 
Situation Analysis 
 
Some lakes in Banten and Bogor are also prone to the same in-
cident as Situ Gintung, due to a lack of maintenance, such as: Situ 
Pondok Genda in Banten, which is located on the side of a road in a 
hilly area, as well as Situ Kedaung in Banten.

###########################################
############### Passage 2 : ###############
###########################################

The assessment of buildings located the nearby rivers and other 
natural reserves, as well as public education campaign need to be 
done. (www.who.int) 
Local residents helped by the volunteers clean-
ing out the debris in the aftermath of the inci-
dent. Photo credit: WHO 
Background view 
 

The 32 foot-high dam of Situ 
Gintung, which was holding back 
around 70 million cubic feet of water 
at the Pesanggrahan river, was built 
up to a century ago, under the 
Dutch colonial rule. 

Little maintenance had been 
carried out on the dam since then, 
and many dams in Indonesia are in 
similar state.

###########################################
############### Passage 3 : ###############
###########################################

2 
 
Inside Story Headline 
Collapsed Dam of Situ Gintung, Tangerang, Banten Province 
On Friday morning, 27th 
of March 2009, at 2:00 am, 
Situ Gintung dam burst out 
in Cirendeu, Ciputat, Tan-
gerang, Banten Province, 
triggered by the heavy tor-
rential rains. 
10 foot-high wave of wa-
ter flushed into a crowded 
neighborhood, submerging 
400 houses. 98 died, 190 injured, 5 missing, and 171 persons be-
came internally displaced. 2 inpatients in Fatmawati Hospital, and 
139 outpatients.

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Question 5 :
How did the Ministry of Health and WHO respond to the Situ Gintung dam collapse?

Réponse 5 :
Based on the provided context information, the Ministry of Health and WHO responded to the Situ Gintung dam collapse by providing integrated Emergency Health Services according to Standard Operating Procedures. They established a command post and temporary evacuation site at Muhammadiyah University of Jakarta and STIE Ahmad Dahlan, and worked closely with local authorities and volunteers to provide medical assistance to those affected by the dam collapse.



########################################################################

********** What is the aim of the Medical Record Management Training mentioned in the document? **********


###########################################
############### Passage 1 : ###############
###########################################

It contains a patient’s 
vital medical information. The summarized information from patient’s records 
would be used in the management and planning of health care services, 
medical research and production of health care statistics. 
This training is to orientate medical record staff on how to manage the 
medical record services in a health care facility in an effective and efficient 
manner so that good documentation of clinical events would contribute to 
good quality data.(www.ino.searo.who.int ) 
 
 
 
 
 
 
 
Photo Credit: WHO 
Indonesia Plans Monitoring System on the Impact of the Economic Crisis on the Poor and 
Vulnerable

###########################################
############### Passage 2 : ###############
###########################################

The crisis is already leading to 
massive layoffs in the manufactur-
ing sector and a sharp drop in re-
mittances from abroad. Public ex-
penditures are being redirected 
from social investments to short-
term economic stimulus packages. 
"The pressure on budgets is likely 
to impact spending on education, 
health, clean water and nutrition, 
investments that are so crucial for 
the attainment of the Millennium 
Development Goals (MDGs)," said 
Mr. Benlamlih. 
 
Medical Record Management Training 
Jakarta (27/03) - Medical record keeping is an essential part of a health 
service delivery in hospitals and public health facilities. It contains a patient’s 
vital medical information.

###########################################
############### Passage 3 : ###############
###########################################

Do you have any comments or views about this 
e-Newsletter? Please send it to e-newsletter@unic-jakarta.org 
 
Published by:  
United Nations Information Centre - Jakarta in Collaboration with the United Nations Communication Group 
This e-Newsletter is for information only

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Question 6 :
What is the aim of the Medical Record Management Training mentioned in the document?

Réponse 6 :
Based on the provided context, the aim of the Medical Record Management Training is to orientate medical record staff on how to manage medical record services in a healthcare facility in an effective and efficient manner, so that good documentation of clinical events would contribute to good quality data.



########################################################################

********** Who are the organizations involved in the production of this document? **********


###########################################
############### Passage 1 : ###############
###########################################

Do you have any comments or views about this 
e-Newsletter? Please send it to e-newsletter@unic-jakarta.org 
 
Published by:  
United Nations Information Centre - Jakarta in Collaboration with the United Nations Communication Group 
This e-Newsletter is for information only

###########################################
############### Passage 2 : ###############
###########################################


WHO and MOH up-dating the 
situation and filling the gaps in col-
laboration with National Board for 
Disaster Management (BNPB). 

WHO is in close communication 
with MOH in monitoring the situation 
through Crisis Center as well as Dis-
trict and Provincial Health Office. 

President of Indonesia has visited the incidence site, along with 
Vice President, Coordinating Minister of People Welfare, Social Af-
fairs Minister, and Health Minister to help the victims.

###########################################
############### Passage 3 : ###############
###########################################

It contains a patient’s 
vital medical information. The summarized information from patient’s records 
would be used in the management and planning of health care services, 
medical research and production of health care statistics. 
This training is to orientate medical record staff on how to manage the 
medical record services in a health care facility in an effective and efficient 
manner so that good documentation of clinical events would contribute to 
good quality data.(www.ino.searo.who.int ) 
 
 
 
 
 
 
 
Photo Credit: WHO 
Indonesia Plans Monitoring System on the Impact of the Economic Crisis on the Poor and 
Vulnerable

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Question 7 :
Who are the organizations involved in the production of this document?

Réponse 7 :
Based on the provided context information, the organizations involved in the production of this document are:

1. United Nations Information Centre - Jakarta
2. United Nations Communication Group
3. National Board for Disaster Management (BNPB)
4. World Health Organization (WHO)
5. Ministry of Health (MOH)
6. President of Indonesia

These organizations are involved in the production and distribution of this e-Newsletter, as mentioned in the context information.



########################################################################

********** Who published this document? **********


###########################################
############### Passage 1 : ###############
###########################################

Do you have any comments or views about this 
e-Newsletter? Please send it to e-newsletter@unic-jakarta.org 
 
Published by:  
United Nations Information Centre - Jakarta in Collaboration with the United Nations Communication Group 
This e-Newsletter is for information only

###########################################
############### Passage 2 : ###############
###########################################

It contains a patient’s 
vital medical information. The summarized information from patient’s records 
would be used in the management and planning of health care services, 
medical research and production of health care statistics. 
This training is to orientate medical record staff on how to manage the 
medical record services in a health care facility in an effective and efficient 
manner so that good documentation of clinical events would contribute to 
good quality data.(www.ino.searo.who.int ) 
 
 
 
 
 
 
 
Photo Credit: WHO 
Indonesia Plans Monitoring System on the Impact of the Economic Crisis on the Poor and 
Vulnerable

###########################################
############### Passage 3 : ###############
###########################################

The ceremony was attended by 
H.E. Aurora Bernaldez Dicenta, Ambassador of Spain 
to Indonesia and El-Mostafa Benlamlih, UN Resident 
Coordinator and UNDP Resident Representative to 
Indonesia. (www.undp.or.id) 
Reports From UNESCAP 
Download PDF Version of Economic and 
Social Survey of Asia and the Pacific 2009 
Wish to know more other 
UNICs/UNIS around the 
w o r l d ?  P l e a s e  v i s i t 
www.unic.un.org or click 
on the image if you are 
viewing it through PDF 
Wish to know more other 
UNICs/UNIS around the world?

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Question 8 :
Who published this document?

Réponse 8 :
The document was published by the United Nations Information Centre - Jakarta in collaboration with the United Nations Communication Group.



########################################################################

