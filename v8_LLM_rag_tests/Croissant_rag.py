import PyPDF2
from langchain_community.embeddings import HuggingFaceEmbeddings
from langchain_community.vectorstores import FAISS, Annoy
from langchain_text_splitters import RecursiveCharacterTextSplitter
from langchain.chains.combine_documents import create_stuff_documents_chain
from langchain_core.prompts import ChatPromptTemplate
from langchain.chains import create_retrieval_chain
from langchain_community.llms.huggingface_pipeline import HuggingFacePipeline
from langchain_community.document_loaders import TextLoader

# plus utile
# pdf_reader = PyPDF2.PdfReader(
#     'annexe-2-licence-professionnelle-bachelor-universitaire-de-technologie-informatique-29016.pdf')
# extracted_text = ''.join(page.extract_text() for page in pdf_reader.pages)
#
# with open("pdf_en_txt.txt", "w", encoding="utf-8") as fichier:
#     fichier.write(extracted_text)

docs = TextLoader(".pdf_en_txt.txt", encoding="utf-8")
documents = docs.load()

text_splitter = RecursiveCharacterTextSplitter(chunk_size=1024, chunk_overlap=200)
documents_split = text_splitter.split_documents(documents)

# prompt = ChatPromptTemplate.from_template("""Réponds à la question suivante en te basant uniquement sur le contexte fourni:
#
# {context}
#
# Question: {input}""")

prompt = ChatPromptTemplate.from_template("""Répond à la question suivante avec le contexte suivant.

Contexte : 
{context}

Question: {input}""")

llm = HuggingFacePipeline.from_model_id(
    model_id="croissantllm/CroissantLLMChat-v0.1",
    task="text-generation",
    device_map="cuda",
    pipeline_kwargs={"max_new_tokens": 40},
    # repetition_penalty=1.1,
)

embeddings = HuggingFaceEmbeddings(model_name="BAAI/bge-small-en-v1.5")

vectorstore = FAISS.from_documents(
    documents_split, embedding=embeddings
)

# # permet d'affichier un par un toutes les parties du texte découpé ainsi que les embeddings correspondants
# num_vectors = vectorstore.index.ntotal
# vectors = [vectorstore.index.reconstruct(i) for i in range(num_vectors)]
#
# for i in range(num_vectors):
#     print("Document:", documents_split[i].page_content)
#     print("Vecteur d'embedding:", vectors[i])
#     print("---")


# on spécifie ici que la recherche se fait par similarité ou mmr et on ajoute un nombre de "contexte" à retenir
# mmr semble plus adapté, il permet de récupérer des contextes plus diversifiés (Maximal Marginal Relevance)
retriever = vectorstore.as_retriever(
    embedding=embeddings,
    search_type="mmr",  # autre possibilité : similarity
    distance_metric="IP",  # L2 pour distance euclidienne et IP pour produit scalaire
    top_k=10,  # nombre de contextes à présélectionner
    # score_threshold=0.5,
    # nombre de contextes à sélectionner, on trie les résultats par ordre de similarité
    search_kwargs={"k": 5},  # "sort": True
)

# print(retriever)

document_chain = create_stuff_documents_chain(llm, prompt)
retrieval_chain = create_retrieval_chain(retriever, document_chain)

# pour faire des tests rapides sur une seule question :

# retrieval_chain.invoke({"input": "Est-ce que des stages sont obligatoires pendant la formation ?"})

reponse = retrieval_chain.invoke({"input": "Est-ce qu'on peut faire des stages à l'étranger ?"})

j = 1

for doc in reponse['context']:
    print("\n=============== Contexte " + str(j) + " : ===============\n" + doc.page_content)
    j += 1

print(reponse['answer'])


# pour faire des tests à plus grande échelle :

i = 1

# # On ouvre le fichier des questions
# with open('Liste_Questions.txt', 'r', encoding='utf-8') as fichier:
#     # on lit chaque ligne du fichier, qui correspondent à une question chacune
#     for ligne in fichier:
#         print("ligne " + str(i))
#
#         # on enlève le \n qui se trouve à la fin de la question !
#         ligne = ligne.replace('\n', '')  # on remplace les \n par rien du tout
#
#         # on génère la réponse de la question
#         reponse = retrieval_chain.invoke({"input": ligne})
#         # on ouvre un nouveau fichier pour y écrire les questions, les réponses que le modèle a générées et les
#         # passages du pdf qui ont servi à produire ces réponses
#         with open('./Questions_Reponses/Questions_et_Reponses_v39.txt', 'a', encoding='utf-8') as fichier2:
#             # on affiche la question et la réponse
#             fichier2.write("Question " + str(i) + " :\n" + ligne + '\n\n'
#                            "Réponse " + str(i) + " :\n" + reponse['answer'] + '\n\n')
#             # on affiche le contexte qui a servi à produire la réponse
#             j = 1
#             fichier2.write("/===============\\ Contextes : /===============\\\n")
#             for doc in reponse['context']:
#                 fichier2.write("\n=============== Contexte " + str(j) + " : ===============\n" + doc.page_content)
#                 j += 1
#
#             fichier2.write('\n\n########################################################################\n\n')
#             i += 1
#
# print("C'est enfin fini !")
