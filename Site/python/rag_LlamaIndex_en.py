from langchain_community.embeddings import HuggingFaceEmbeddings
from llama_index.core import VectorStoreIndex, Settings
from llama_index.core.indices.vector_store import VectorIndexRetriever
from llama_index.core.query_engine import RetrieverQueryEngine
from llama_index.llms.ollama import Ollama
from llama_index.readers.file import PyMuPDFReader

def load_index():
    documents = PyMuPDFReader().load("pdf/Eloquent_JavaScript.pdf")

    embedding_model = HuggingFaceEmbeddings(model_name="BAAI/bge-small-en-v1.5")

    index = VectorStoreIndex.from_documents(
        documents,
        embed_model=embedding_model,
        show_progress=True,
    )

    llama = Ollama(
        model="llama2",
        request_timeout=600.0,
    )

    retriever = VectorIndexRetriever(
        index=index,
        similarity_top_k=3,
    )

    Settings.chunk_size = 150
    Settings.chunk_overlap = 10
    Settings.embed_model = embedding_model
    Settings.num_output = 10
    Settings.llm = llama

    query_engine = RetrieverQueryEngine(
        retriever=retriever
    )

    return query_engine


def query(query_engine, question):
    return str(query_engine.query(question))
