from flask import Flask, request, jsonify
from rag_LlamaIndex_fr_v2 import load_index, query  # on indique le fichier à importer
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

query_engine = load_index()


@app.route('/api/query', methods=['POST'])
def handle_query():
    question = request.json['question']
    response = query(query_engine, question)

    return jsonify({'response': str(response)})


if __name__ == '__main__':
    app.run()
