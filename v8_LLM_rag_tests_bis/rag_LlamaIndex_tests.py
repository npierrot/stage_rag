# https://huggingface.co/learn/cookbook/en/rag_llamaindex_librarian

# ne pas oublier de lancer "ollama serve" dans un terminal avant de lancer le programme

from llama_index.core import SimpleDirectoryReader, PromptTemplate, ChatPromptTemplate, Prompt
from langchain_community.embeddings import HuggingFaceEmbeddings
from llama_index.core import VectorStoreIndex, Settings
from llama_index.core import StorageContext, load_index_from_storage
from llama_index.core.base.llms.types import ChatMessage, MessageRole
from llama_index.core.callbacks import LlamaDebugHandler, CallbackManager, CBEventType
from llama_index.core.indices.vector_store import VectorIndexRetriever
from llama_index.core.prompts import PromptType
from llama_index.core.prompts.prompts import QuestionAnswerPrompt
from llama_index.core.query_engine import RetrieverQueryEngine
from llama_index.core.vector_stores.types import VectorStoreQueryMode
from llama_index.llms.ollama import Ollama
from llama_index.readers.file import PyMuPDFReader

# import subprocess
# # commande pour lancer le serveur local
# command = "ollama serve"
# # on execute la commande
# process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
# print("\"ollama serve\" lancé")

import logging
import sys

from llama_index.readers.file import PyMuPDFReader

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logging.getLogger().addHandler(logging.StreamHandler(stream=sys.stdout))

import llama_index.core
from llama_index.core import set_global_handler

llama_index.core.set_global_handler("simple")
set_global_handler("simple")

# documents = SimpleDirectoryReader(
#     input_dir="./test/",
#     recursive=True,
#     required_exts=[".epub"],
# ).load_data()
# print("documents chargés")

file_path = "pdf/annexe-2-licence-professionnelle-bachelor-universitaire-de-technologie-informatique-29016.pdf"
reader = PyMuPDFReader()
documents = reader.load(file_path)
# print("document loaded")

# https://huggingface.co/models?pipeline_tag=feature-extraction&language=fr&sort=downloads
# sentence-transformers/paraphrase-multilingual-MiniLM-L12-v2
# intfloat/multilingual-e5-large
embedding_model = HuggingFaceEmbeddings(model_name="intfloat/multilingual-e5-large")

# print("embeddings chargés")

Settings.chunk_size = 200  # taille des passages
Settings.chunk_overlap = 20  # superposition entre 2 passages
Settings.embed_model = embedding_model  # modèle d'embeddings
Settings.num_output = 10  # taille de la réponse (en tokens)

llama_debug = LlamaDebugHandler(print_trace_on_end=True)
callback_manager = CallbackManager([llama_debug])


index = VectorStoreIndex.from_documents(
    documents,
    embed_model=embedding_model,
    # callback_manager=callback_manager,
    show_progress=True,  # pour voir la progression du chargement
)

# print("VectorStoreIndex chargé")

# pour sauvegarder l'index avec ses paramètres
# index.storage_context.persist(persist_dir="sauvegarde_index_but_fr")
#
# storage_context = StorageContext.from_defaults(persist_dir="sauvegarde_index_but_fr")
#
# index = load_index_from_storage(storage_context)

# from transformers import AutoTokenizer, AutoModelForCausalLM
#
# tokenizer = AutoTokenizer.from_pretrained("bofenghuang/vigogne-2-7b-instruct")
# model = AutoModelForCausalLM.from_pretrained("bofenghuang/vigogne-2-7b-instruct")

llama = Ollama(
    model="mistral",
    request_timeout=600.0,
)
# print("llm chargé")

# https://github.com/run-llama/llama_index/discussions/8423
# https://github.com/run-llama/llama_index/blob/af24c02343c3662983e59ebe078fce518a3a212f/llama_index/prompts/default_prompts.py#L98
chat_text_qa_msgs = [
    ChatMessage(
        role=MessageRole.SYSTEM,
        content=(
            "Vous êtes un système expert de questions-réponses auquel le monde entier fait confiance.\n"
            "Répondez toujours à la requête en utilisant les informations contextuelles fournies"
            "et non vos connaissances préalables.\n"
            "Quelques règles à suivre :\n"
            "1. Ne faites jamais directement référence au contexte donné dans votre réponse.\n"
            "2. Évitez les déclarations telles que \"D'après le contexte, ...\" ou "
            "\"Les informations contextuelles ...\" ou toute autre déclaration de ce type."
        ),
    ),
    ChatMessage(
        role=MessageRole.USER,
        content=(
            "Les informations contextuelles sont ci-dessous.\n"
            "---------------------\n"
            "{context_str}\n"
            "---------------------\n"
            "Compte tenu des informations contextuelles et sans connaissances préalables, répondez à la requête.\n"
            "Requête :{query_str}\n"
            "Réponse : "
        ),
    ),
]
text_qa_template = ChatPromptTemplate(chat_text_qa_msgs)

# first_prompt = ("Vous êtes un système expert de questions-réponses auquel le monde entier fait confiance.\n"
#                 "Répondez toujours à la requête en utilisant les informations contextuelles fournies"
#                 "et non vos connaissances préalables.\n"
#                 "Quelques règles à suivre :\n"
#                 "1. Ne faites jamais directement référence au contexte donné dans votre réponse.\n"
#                 "2. Évitez les déclarations telles que \"D'après le contexte, ...\" ou "
#                 "\"Les informations contextuelles ...\" ou toute autre déclaration de ce type.")
#
# second_prompt = ("Les informations contextuelles sont ci-dessous.\n"
#                  "---------------------\n"
#                  "{context_str}\n"
#                  "---------------------\n"
#                  "Compte tenu des informations contextuelles et sans connaissances préalables, répondez à la requête.\n"
#                  "Requête :{query_str}\n"
#                  "Réponse : ")

# DEFAULT_REFINE_PROMPT = Prompt(
#     first_prompt, prompt_type=PromptType.REFINE
# )
# refine_prompt = Prompt(first_prompt)

# DEFAULT_TEXT_QA_PROMPT = Prompt(
#     second_prompt, prompt_type=PromptType.QUESTION_ANSWER
# )
# TEXT_QA_TEMPLATE = QuestionAnswerPrompt(second_prompt)

# Settings.llm.set_messages_to_prompt
# Settings.text_splitter = text_qa_template

# qa_prompt_tmpl_str = (
#     "Les informations sur le contexte se trouvent ci-dessous.\n"
#     "---------------------\n"
#     "{context_str}"
#     "---------------------\n"
#     "Compte tenu des informations contextuelles et sans connaissances préalables,"
#     "répondre à la requête, en français.\n"
#     "Requête : {query_str}\n"
#     "Réponse : "
# )
# qa_prompt_tmpl = PromptTemplate(qa_prompt_tmpl_str)

retriever = VectorIndexRetriever(
    # show_progress=True,
    index=index,
    # vector_store_query_mode="mmr",  # je pense que ça fonctionne pas
    similarity_top_k=3,  # nombre de passages récupérés
    # prompt_template_str=text_qa_template
    # refine_template=text_qa_template
)

# query_engine = index.as_query_engine(vector_store_query_mode="mmr")

# Settings.llm = llama
# # Settings.llm.system_prompt = text_qa_template
#
# query_engine = RetrieverQueryEngine(
#     retriever=retriever
# )

# chat_refine_msgs = [
#     ChatMessage(
#         role=MessageRole.SYSTEM,
#         content=(
#             "Vous êtes un système expert de questions-réponses qui fonctionne strictement selon deux modes "
#             "lorsqu'il s'agit d'affiner des réponses existantes :\n"
#             "1. **Réécrire** une réponse originale en tenant compte du nouveau contexte.\n"
#             "2. **Répéter** la réponse originale si le nouveau contexte n'est pas utile.\n"
#             "Ne faites jamais référence à la réponse originale ou au contexte directement dans votre réponse.\n"
#             "En cas de doute, répétez simplement la réponse originale."
#         ),
#     ),
#     ChatMessage(
#         role=MessageRole.USER,
#         content=(
#             "Nouveau Contexte: {context_msg}\n"
#             "Requête: {query_str}\n"
#             "Réponse Originale: {existing_answer}\n"
#             "Nouvelle Réponse: "
#         ),
#     ),
# ]
# refine_template = ChatPromptTemplate(chat_refine_msgs)
# print("refine_template ok")


query_engine = RetrieverQueryEngine(
    retriever=retriever,
).from_args(
    retriever=retriever,
    text_qa_template=text_qa_template,
    # refine_template=refine_template,
)
# print("query prête")

# results = retriever.retrieve("Est ce que les stages sont obligatoires ?")
# i = 1
# for result in results:
#     print("###########################################\n"
#           "############### Passage " + str(i) + " : ###############\n"
#           "###########################################")
#     print(result.node.text)
#     i += 1

print(query_engine.query("Qu'est-ce que sont les SAE ?"))

# exit()

# print("On arrive à la phase finale")


i = 1
# On ouvre le fichier des questions
with open('questions_reponses_Jakarta/questions_en.txt', 'r', encoding='utf-8') as fichier:
    # on lit chaque ligne du fichier, qui correspondent à une question chacune
    for ligne in fichier:
        print("ligne " + str(i))
        # on enlève le \n qui se trouve à la fin de la question
        ligne = ligne.replace('\n', '')  # on supprime les retours à la ligne
        # on génère la réponse de la question
        rep = query_engine.query(ligne)
        # on ouvre un nouveau fichier pour y écrire les questions, les réponses que le modèle a générées et les
        # passages du pdf qui ont servi à produire ces réponses
        with open('./questions_reponses_Jakarta/questions_reponses5.txt', 'a', encoding='utf-8') as fichier2:
            results = retriever.retrieve(ligne)
            j = 1
            # on écrit la question
            fichier2.write("********** " + ligne + " **********\n\n")
            # on affiche tous les contextes qui ont servi à répondre à la question
            for result in results:
                fichier2.write("\n###########################################\n"
                               "############### Passage " + str(j) + " : ###############\n"
                               "###########################################\n\n")
                fichier2.write(result.node.text)
                fichier2.write("\n")
                j += 1
            # on affiche la question et la réponse
            fichier2.write("\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
                           "Question " + str(i) + " :\n" + ligne + '\n\n'
                           "Réponse " + str(i) + " :\n" + str(rep) + '\n\n')
            fichier2.write('\n\n########################################################################\n\n')
            i += 1
#
# print("C'est enfin fini !")

# from llama_index.core import PromptTemplate
#
# template = (
#     "We have provided context information below. \n"
#     "---------------------\n"
#     "{context_str}"
#     "\n---------------------\n"
#     "Given this information, please answer the question: {query_str}\n"
# )
# qa_template = PromptTemplate(template)



# from llama_index.core.postprocessor import (
#     NERPIINodePostprocessor,
#     SentenceEmbeddingOptimizer,
# )
# from llama_index.core import QueryBundle
# from llama_index.core.schema import NodeWithScore, TextNode
#
# pii_processor = NERPIINodePostprocessor(llm=llama)
#
# def filter_pii_fn(**kwargs):
#     # run optimizer
#     query_bundle = QueryBundle(query_str=kwargs["query_str"])
#
#     new_nodes = pii_processor.postprocess_nodes(
#         [NodeWithScore(node=TextNode(text=kwargs["context_str"]))],
#         query_bundle=query_bundle,
#     )
#     new_node = new_nodes[0]
#     return new_node.get_content()
#
# qa_prompt_tmpl_str = (
#     "Les informations sur le contexte se trouvent ci-dessous.\n"
#     "---------------------\n"
#     "{context_str}"
#     "---------------------\n"
#     "Compte tenu des informations contextuelles et sans connaissances préalables,"
#     "répondre à la requête.\n"
#     "Requête : {query_str}\n"
#     "Réponse : "
# )
#
# qa_prompt_tmpl = PromptTemplate(
#     qa_prompt_tmpl_str, function_mappings={"context_str": filter_pii_fn}
# )
#
# query_engine.update_prompts(
#     {"response_synthesizer:text_qa_template": qa_prompt_tmpl}
# )
#
# print("Réponse 0.5 : ")
# print(str(query_engine.query("Est-ce que des stages sont obligatoires pendant la formation ?")))


# print("Réponse 1 : ")
# print(query_engine.query("Who is Carla?"))
#
# print("Réponse 2 : ")
# print(query_engine.query("What is Conway’s Game of Life?"))
#
# print("Réponse 3 : ")
# print(query_engine.query("What are the projects in this document?"))
#
# print("Réponse 4 : ")
# print(query_engine.query("What is the subject of this document?"))

# print("Réponse 5 : ")
# print(query_engine.query("Who is the author of this document?"))

intro = "Posez une question :\n-> "
msg = input(intro)
while msg != 'exit':
    results = retriever.retrieve(msg)
    i = 1
    for result in results:
        print("###########################################\n"
              "############### Passage " + str(i) + " : ###############\n"
              "###########################################")
        print(result.node.text)
        print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
        i += 1
    print(query_engine.query(msg))
    msg = input(intro)




# print("Réponse 1 : ")
# print(query_engine.query("What is the theme of the document?"))
#
# print("Réponse 2 : ")
# print(query_engine.query("Give me a summary of the document."))

# print("Réponse 3 : ")
# print(query_engine.query("Who took the photos in the document?"))
#
# print("Réponse 4 : ")
# print(query_engine.query("What can you tell me about the dam that collapsed?"))

# print("Réponse 5 : ")
# print(
#     query_engine.query(
#         "Who were the speakers at the launch of the Economic and Social Survey for Asia and the Pacific?"
#     )
# )

# print("Réponse 6 : ")
# print(query_engine.query("What caused the collapse of the Situ Gintung dam in Tangerang, Banten Province?"))

# print("Réponse 7 : ")
# print(query_engine.query("How did the Ministry of Health and WHO respond to the Situ Gintung dam collapse?"))

# print("Réponse 8 : ")
# print(query_engine.query("What is the aim of the Medical Record Management Training mentioned in the document?"))

# print("Réponse 9 : ")
# print(
#     query_engine.query(
#         "What is the purpose of the technical workshop hosted by Indonesia's National Development Planning Agency ("
#         "Bappenas) and several UN agencies?"
#     )
# )

# print("Réponse 10 : ")
# print(query_engine.query("What is the main topic of this document?"))

# print("Réponse 11 : ")
# print(query_engine.query("Who are the organizations involved in the production of this document?"))

# print("Réponse 12 : ")
# print(query_engine.query("What are the main themes addressed in the Economic and Social Survey for Asia and the Pacific 2009?"))

# print("Réponse 13 : ")
# print(query_engine.query("Give me 3 names of the authors of the photos in the document."))






# print("Réponse 1 : ")
# print(query_engine.query("Did Gargantua come out of his mother's left ear?"))
#
# print("Réponse 2 : ")
# res = query_engine.query("What is the name of Gargantua's mother?")
# print(res)
#
# # # Print info on the LLM calls during the summary index query
# # print(llama_debug.get_event_time_info(CBEventType.LLM))
# # Print info on llm inputs/outputs - returns start/end events for each LLM call
# # event_pairs = llama_debug.get_llm_inputs_outputs()
# # print(event_pairs[0][0])
# # print(event_pairs[0][1].payload.keys())
# # print(event_pairs[0][1].payload["response"])
# print("Réponse 2 bis : ")
# print(query_engine.query("What is the name of Gargantua's mother? Show me the context used to derive your answer."))
#
# print("Réponse 3 : ")
# print(query_engine.query("What is Friar John's role?"))
#
# print("Réponse 4 : ")
# print(query_engine.query("Why is Gargantua called Gargantua?"))

# Réponse 1 :
# I cannot directly reference the given context in my answer as per the rules provided. However, I can provide a creative and humorous response to the query without directly referencing the context information. Here it is:
#
# Oh my goodness, no! Gargantua certainly didn't come out of his mother's left ear. Well, at least not that I know of. After all, if he had, he would have been a bit... unusual-looking, don't you think? I mean, can you imagine a giant with an ear on top of his head? It would be quite the sight! 😂
#
# But seriously, where did Gargantua come from? Well, according to Plinius himself, he was born in a most unusual manner. It seems that Leda, his mother, had laid an egg which hatched into him! Can you imagine? An egg laying a baby giant? 😲 It's enough to make your head spin!
#
# Anyway, I hope that answers your query. Or did it just leave you with more questions? 🤔
# Réponse 2 :
# I must respectfully decline to provide the name of Gargantua's mother based on the context information provided. The query directly references the text and asks for information that is not explicitly stated in the given passage. As an expert Q&A system, I cannot provide an answer that goes beyond the information provided in the context.
#
# The passage mentions Grangousier's attempt to comfort Gargamelle during her labor pains, but it does not provide any information about Gargantua's mother. Therefore, I cannot provide a definitive answer to the query.
# Réponse 2 bis :
# The name of Gargantua's mother is Gargamelle. This can be derived from the context provided as follows:
#
# In the passage, Gargamelle is mentioned as the mother of Gargantua. The sentence "Whilst they were on this discourse and pleasant tattle of drinking, Gargamelle began to be a little unwell in her lower parts" clearly identifies Gargamelle as the mother of Gargantua.
#
# Therefore, the answer to the query is: Gargamelle.
# Réponse 3 :
# Based on the context provided, Friar John appears to be a trusted advisor or friend to Gargantua. He is mentioned as being present during a conversation between Gargantua and another character, Grangousier, and is shown to be welcomed and received with courtesy by Gargantua. Friar John's role may involve providing guidance or advice to Gargantua, as well as serving as a confidant or companion.
# Réponse 4 :
# Based on the provided context, it can be inferred that Gargantua is called Gargantua because the name was given to him by his father, Grangousier. According to the text, Grangousier heard a discourse about Gargantua's intelligence and marvelled at his son's high reach and understanding. It is likely that Grangousier gave Gargantua the name "Gargantua" as a way of acknowledging and celebrating his exceptional abilities.
