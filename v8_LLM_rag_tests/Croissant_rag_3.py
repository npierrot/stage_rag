import PyPDF2
import torch
from langchain_community.embeddings import HuggingFaceEmbeddings
from langchain_community.vectorstores import FAISS
from langchain_core.output_parsers import StrOutputParser
from langchain_core.runnables import RunnablePassthrough
from langchain_text_splitters import RecursiveCharacterTextSplitter
from langchain.chains.combine_documents import create_stuff_documents_chain
from langchain_core.prompts import ChatPromptTemplate, PromptTemplate
from langchain.chains import create_retrieval_chain
from langchain_community.llms.huggingface_pipeline import HuggingFacePipeline
from langchain_community.document_loaders import TextLoader
from transformers import BitsAndBytesConfig, AutoModelForCausalLM, AutoTokenizer, pipeline

# plus utile
# pdf_reader = PyPDF2.PdfReader(
#     'annexe-2-licence-professionnelle-bachelor-universitaire-de-technologie-informatique-29016.pdf')
# extracted_text = ''.join(page.extract_text() for page in pdf_reader.pages)
#
# with open("pdf_en_txt.txt", "w", encoding="utf-8") as fichier:
#     fichier.write(extracted_text)
docs = TextLoader("./pdf_en_txt_sans_sommaire.txt", encoding="utf-8")
documents = docs.load()

splitter = RecursiveCharacterTextSplitter(chunk_size=300, chunk_overlap=100)
documents_split = splitter.split_documents(documents)

db = FAISS.from_documents(
    documents_split, embedding=HuggingFaceEmbeddings(model_name="all-MiniLM-L6-v2")
)

retriever = db.as_retriever(
    search_type="mmr",
    search_kwargs={"k": 5},
)

model_name = "croissantllm/CroissantLLMChat-v0.1"

# bnb_config = BitsAndBytesConfig(
#     load_in_4bit=True, bnb_4bit_use_double_quant=True, bnb_4bit_quant_type="nf4", bnb_4bit_compute_dtype=torch.bfloat16
# )

model = AutoModelForCausalLM.from_pretrained(model_name)  # quantization_config=bnb_config
tokenizer = AutoTokenizer.from_pretrained(model_name)

text_generation_pipeline = pipeline(
    model=model,
    tokenizer=tokenizer,
    task="text-generation",
    temperature=0.2,
    do_sample=True,
    repetition_penalty=1.1,
    return_full_text=True,
    max_new_tokens=50,
)

llm = HuggingFacePipeline(pipeline=text_generation_pipeline)

prompt_template = """Réponds à la question suivante en te basant uniquement sur le contexte fourni :

{context}

Question : {question}"""

prompt = PromptTemplate(
    input_variables=["context", "question"],
    template=prompt_template,
)

llm_chain = prompt | llm | StrOutputParser()

retriever = db.as_retriever()

rag_chain = {"context": retriever, "question": RunnablePassthrough()} | llm_chain

print(rag_chain.invoke("Combien de temps durent les stages ?"))


# i = 1
#
# # On ouvre le fichier des questions
# with open('Liste_Questions.txt', 'r', encoding='utf-8') as fichier:
#     # on lit chaque ligne du fichier, qui correspondent à une question chacune
#     for ligne in fichier:
#         print("ligne " + str(i))
#         # on génère la réponse de la question
#         reponse = rag_chain.invoke(ligne)
#         # on ouvre un nouveau fichier pour y écrire les questions, les réponses que le modèle a générées et les
#         # passages du pdf qui ont servi à produire ces réponses
#         with open('./Comparaison_retriever_1/Annoy_Questions_et_Reponses.txt', 'a', encoding='utf-8') as fichier2:
#             # on affiche la question et la réponse
#             fichier2.write("Question " + str(i) + " :\n" + ligne + '\n'
#                            "Réponse " + str(i) + " :\n" + reponse['answer'] + '\n\n')
#             # on affiche le contexte qui a servi à produire la réponse
#             j = 1
#             fichier2.write("/===============\\ Contextes : /===============\\\n")
#             for doc in reponse['context']:
#                 fichier2.write("\n=============== Contexte " + str(j) + " : ===============\n" + doc.page_content)
#                 j += 1
#
#             fichier2.write('\n\n########################################################################\n\n')
#             i += 1
#
# print("C'est enfin fini !")
