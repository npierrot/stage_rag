from langchain_community.embeddings import HuggingFaceEmbeddings
from llama_index.embeddings.huggingface import HuggingFaceEmbedding
from llama_index.core import SimpleDirectoryReader
from llama_index.core import VectorStoreIndex
from llama_index.llms.ollama import Ollama
import llama_index.core
from llama_index.core import Settings
from llama_index.readers.file import PyMuPDFReader
import sys

from llama_index.core.callbacks import (
    CallbackManager,
    LlamaDebugHandler,
    CBEventType,
)

llama_debug = LlamaDebugHandler(print_trace_on_end=True)
callback_manager = CallbackManager([llama_debug])

import logging

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logging.getLogger().addHandler(logging.StreamHandler(stream=sys.stdout))

llama_index.core.set_global_handler("simple")

embedding_model = HuggingFaceEmbeddings(model_name="BAAI/bge-small-en-v1.5")
llama = Ollama(
    model="llama2",
    request_timeout=600.0,
)
print("ollama started")

Settings.llm = llama
Settings.embed_model = embed_model = embedding_model
Settings.callback_manager = callback_manager

# loader = SimpleDirectoryReader(
#     input_dir="./test/",
#     recursive=True,
#     required_exts=[".epub"],
# )
# documents = loader.load_data()

file_path = "pdf/Bel-ami.pdf"
reader = PyMuPDFReader()
documents = reader.load(file_path)
print("document loaded")

index = VectorStoreIndex.from_documents(documents)
index.storage_context.persist('saved_index.json')
print("docs indexed")

query_engine = index.as_query_engine()
print("queryengine")

response = query_engine.query("What are the names of all the women Georges Duroy has had?")
print("RESPONSE", response)

# Print info on llm inputs/outputs - returns start/end events for each LLM call
print(llama_debug.get_event_time_info(CBEventType.LLM))
event_pairs = llama_debug.get_llm_inputs_outputs()
print("******************** AVANT 1 ********************")
print(event_pairs[0][0])
print("******************** VOITURE ********************")
print(event_pairs[0][1].payload.keys())
print("******************** FROMAGE ********************")
print(event_pairs[0][1].payload["response"])
