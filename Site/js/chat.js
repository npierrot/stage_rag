// on récupère les éléments du document qui nous intéressent
let listeReponses = document.getElementById("chat_reponses");
let barreRecherche = document.getElementById("barreRecherche")

function affichage(question, reponse, id_temp_qa = ""){
    // création de la div pour les questions / réponses
    let qa = document.createElement("div");
    qa.className = "qa";

    // création du paragraphe pour les questions
    let afficherQuestion = document.createElement("p");
    afficherQuestion.className = "question";
    if(id_temp_qa != ""){
        afficherQuestion.id = id_temp_qa;
    }
    afficherQuestion.textContent = question;
    qa.appendChild(afficherQuestion);

    //création du paragrpahe pour la réponse
    let afficherReponse = document.createElement("p");
    afficherReponse.className = "reponse";
    afficherReponse.textContent = reponse;
    qa.appendChild(afficherReponse);

    // on affiche la question / réponse au-dessus des précédentes
    let fChild = listeReponses.firstChild;
    listeReponses.insertBefore(qa, fChild);
}

function recupererReponse(){
    question = barreRecherche.value;
    barreRecherche.value = "";
    // on desactive la barre de recherche pendant la génération de la réponse
    barreRecherche.placeholder = "Réponse en cours de génération...";
    barreRecherche.readOnly = true;
    // affichage provisoir pour annoncer la génération de la réponse
    affichage(question, "...", "qa_temp");

    // on envoie une requête POST avec la question
    fetch('http://localhost:5000/api/query', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({'question': question})
    })
    .then(response => response.json())
    .then(data => {
        // on traite la réponse et on l'affiche
        let reponse = data.response;
        //on retire l'affichage provisoire
        listeReponses.removeChild(listeReponses.firstChild)
        // on affiche la vrai réponse
        affichage(question, reponse);

        // on reset et on réactive la barre de recherche
        barreRecherche.placeholder = "Posez votre question...";
        barreRecherche.readOnly = false;
    })
    .catch(error => {
        alert("Une erreur s'est produite. Veillez à ce que le serveur ollama soit bien lancé et que le script app.py soit bien en train de tourner. Ensuite rechargez la page.");
    });
}

window.addEventListener("load", () => {
    barreRecherche.addEventListener("keydown", function(event) {
        if (event.key === "Enter") {
            event.preventDefault(); // ça empêche le formulaire d'être soumis (action par défaut d'un formulaire)
            recupererReponse(); 
        }
    });

});
