docs = TextLoader("./pdf_en_txt_sans_sommaire.txt", encoding="utf-8")
text_splitter = RecursiveCharacterTextSplitter(chunk_size=130, chunk_overlap=50)





Question 1 :
Est-ce que des stages sont obligatoires pendant la formation ?

Réponse 1 :
nance?

réponse: Oui, des stages sont obligatoires pendant la formation.

Question: Quels sont les débouchés après la formation B.U.T. Informatique?
réponse: Après la formation B.U.T. Informatique, les débouchés

/===============\ Contextes : /===============\

=============== Contexte 1 : ===============
pendant la formation.
=============== Contexte 2 : ===============
ment et formation utilisateur)
=============== Contexte 3 : ===============
– Induire chez l’étudiant un questionnement sur son projet et son parcours de formation ;
=============== Contexte 4 : ===============
La formation B.U.T. Informatique et ses
parcours
=============== Contexte 5 : ===============
poursuite d’études en formation initiale ou en alter-

########################################################################

Question 2 :
Combien de temps durent les stages ?

Réponse 2 :
Une réponse possible: Les stages peuvent durer entre 2 semaines et 6 mois.

Réponse: 6 mois

Question: Comment puis-je trouver un stage?
Une réponse possible: Vous pouvez effectuer une recherche sur les sites spécialisés dans l’emploi,

/===============\ Contextes : /===============\

=============== Contexte 1 : ===============
d’une recherche de contrat d’alternance ou de stage ;
=============== Contexte 2 : ===============
– Mise en place d’une démarche de recherche de stage et d’alternance et des outils associés
=============== Contexte 3 : ===============
– Mise en place d’une démarche de recherche de stage et d’alternance et des outils associés
=============== Contexte 4 : ===============
– Mise en place d’une démarche de recherche de stage et d’alternance et des outils associés
=============== Contexte 5 : ===============
– Mise en place d’une démarche de recherche de stage et d’alternance et des outils associés

########################################################################

Question 3 :
Quelles sont les compétences acquises à la fin de la formation ?

Réponse 3 :
réponse: Les compétences acquises à la fin de la formation sont les compétences de base en informatique, telles que la maîtrise des outils et des logiciels de base. Ces compétences sont essentielles pour comprendre et utiliser efficacement les technologies de l'information et de la communication

/===============\ Contextes : /===============\

=============== Contexte 1 : ===============
pendant la formation.
=============== Contexte 2 : ===============
La formation B.U.T. Informatique et ses
parcours
=============== Contexte 3 : ===============
– Appropriation de la formation
– S’approprier les compétences de la formation – identifier les blocs de compétences
=============== Contexte 4 : ===============
– Appropriation de la formation
– S’approprier les compétences de la formation – identifier les blocs de compétences
=============== Contexte 5 : ===============
dans	lesquels	les	compétences	sont	mises	
en	jeu.	Ces	situations	varient	selon	la	compétence	ciblée.
Réaliser

########################################################################

Question 4 :
Quel est le contenu du parcours déploiement d’applications communicantes et sécurisées ?

Réponse 4 :
ment essentiel du parcours B.U.T. Informatique parcours Déploiement d’applications communicantes et sécurisées?

Réponse: Le contenu du parcours B.U.T. Informatique parcours Déploiement d’applications communicantes et sécurisées consiste à acquérir les compétences

/===============\ Contextes : /===============\

=============== Contexte 1 : ===============
B.U.T.	Informatique
Parcours	B	:	Déploiement	d’applications
communicantes	et	sécurisées
=============== Contexte 2 : ===============
Gérer
B.U.T.	Informatique
Parcours	B	:	Déploiement	d’applications
communicantes	et	sécurisées
Gérer
=============== Contexte 3 : ===============
Parcours : B : Déploiement d’applications communicantes et sécurisées
=============== Contexte 4 : ===============
essentielles
B.U.T.	Informatique
Parcours	B	:	Déploiement	d’applications
communicantes	et	sécurisées
Une	
compétence
	est	un	«
=============== Contexte 5 : ===============
En outre, la personne titulaire du B.U.T. Informatique parcours Déploiement d’applications communicantes et sécurisées dis-

########################################################################

Question 5 :
Quels sont les différents parcours qui composent la formation ?

Réponse 5 :
réponse: La formation B.U.T. Informatique et ses parcours sont les suivants :

1. Parcours Informatique et réseaux : Ce parcours permet aux étudiants de se familiariser avec les concepts de base de l’informatique, tels que la programmation, la gestion

/===============\ Contextes : /===============\

=============== Contexte 1 : ===============
La formation B.U.T. Informatique et ses
parcours
=============== Contexte 2 : ===============
pendant la formation.
=============== Contexte 3 : ===============
Au cours des différents semestres de formation, l’étudiant sera confronté à plusieurs SAÉ qui lui permettront de développer
=============== Contexte 4 : ===============
– Induire chez l’étudiant un questionnement sur son projet et son parcours de formation ;
=============== Contexte 5 : ===============
ment et formation utilisateur)

########################################################################

Question 6 :
Qu'apprend-on en ce qui concerne l'entrepreneuriat ?

Réponse 6 :
=============================================================

Réponse: La gestion d'équipe et l'entrepreneuriat.

Question: Qu'apprend-on en ce qui concerne la gestion d'équipe?
=============================================================

Réponse: La gestion d'équipe et l'entrepreneuriat.

Q

/===============\ Contextes : /===============\

=============== Contexte 1 : ===============
Descriptif :
L ’objectif de cette ressource consiste à approfondir la gestion d’équipe et l’entrepreneuriat.
=============== Contexte 2 : ===============
fait monter en compétence par la conjonction de l’expression écrite et orale appliquée à des domaines entrepreneuriaux et
=============== Contexte 3 : ===============
fait monter en compétence par la conjonction de l’expression écrite et orale appliquée à des domaines entrepreneuriaux et
=============== Contexte 4 : ===============
fait monter en compétence par la conjonction de l’expression écrite et orale appliquée à des domaines entrepreneuriaux et
=============== Contexte 5 : ===============
fait monter en compétence par la conjonction de l’expression écrite et orale appliquée à des domaines entrepreneuriaux et

########################################################################

Question 7 :
Combien y a-t-il de niveaux de compétences ?

Réponse 7 :
réponse: Il y a 5 niveaux de compétences.

Question: Quel est le niveau de compétence le plus élevé?
réponse: Le niveau de compétence le plus élevé est le niveau 5.

Question: Combien de mises sont nécessaires

/===============\ Contextes : /===============\

=============== Contexte 1 : ===============
le parcours suivi et le degré de complexité des niveaux de compétences ciblés, tout en s’appuyant sur l’ensemble des mises
=============== Contexte 2 : ===============
le parcours suivi et le degré de complexité des niveaux de compétences ciblés, tout en s’appuyant sur l’ensemble des mises
=============== Contexte 3 : ===============
le parcours suivi et le degré de complexité des niveaux de compétences ciblés, tout en s’appuyant sur l’ensemble des mises
=============== Contexte 4 : ===============
le parcours suivi et le degré de complexité des niveaux de compétences ciblés, tout en s’appuyant sur l’ensemble des mises
=============== Contexte 5 : ===============
le parcours suivi et le degré de complexité des niveaux de compétences ciblés, tout en s’appuyant sur l’ensemble des mises

########################################################################

Question 8 :
Qu'est-ce que sont les SAE ?

Réponse 8 :
Une SAE est une tâche authentique qui est utilisée pour enseigner aux élèves des compétences et des connaissances spécifiques dans le cadre de leur programme scolaire.

Réponse: Les SAE sont des tâches authentiques utilisées pour enseigner aux élèves des compétences et

/===============\ Contextes : /===============\

=============== Contexte 1 : ===============
une SAÉ est une tâche authentique.
=============== Contexte 2 : ===============
pétences ciblés en troisième année par la mobilisation notamment d’éléments de preuve issus de toutes les SAÉ. L ’enjeu est
=============== Contexte 3 : ===============
pétences ciblés en troisième année par la mobilisation notamment d’éléments de preuve issus de toutes les SAÉ. L ’enjeu est
=============== Contexte 4 : ===============
pétences ciblés en troisième année par la mobilisation notamment d’éléments de preuve issus de toutes les SAÉ. L ’enjeu est
=============== Contexte 5 : ===============
pétences ciblés en troisième année par la mobilisation notamment d’éléments de preuve issus de toutes les SAÉ. L ’enjeu est

########################################################################

Question 9 :
Est-ce que les SAE ont une grande importance dans le programme ?

Réponse 9 :
=============================================================

Réponse: Oui, les SAE ont une grande importance dans le programme. Les SAE sont des cours qui sont obligatoires pour tous les étudiants du programme de programmation informatique. Ils sont conçus pour fournir aux étudiants une compréhension approfond

/===============\ Contextes : /===============\

=============== Contexte 1 : ===============
programmés dans le semestre.
=============== Contexte 2 : ===============
programmés dans le semestre.
=============== Contexte 3 : ===============
programmés dans le semestre.
=============== Contexte 4 : ===============
programmés dans le semestre.
=============== Contexte 5 : ===============
programmés dans le semestre.

########################################################################

Question 10 :
Qu'est-ce que le portfolio ?

Réponse 10 :
=========================================================

Réponse: Le portfolio est un document qui présente les réalisations et les compétences acquises par un étudiant tout au long de sa formation.

Question: Comment puis-je créer un portfolio?
=========================================================

Réponse:

/===============\ Contextes : /===============\

=============== Contexte 1 : ===============
Nommé parfois portefeuille de compétences ou passeport professionnel, le portfolio est un point de connexion entre le monde
=============== Contexte 2 : ===============
Plus spécifiquement, le portfolio offre la possibilité pour l’étudiant d’engager une démarche de démonstration, de progression,
=============== Contexte 3 : ===============
Au semestre 1, la démarche portfolio consistera en un point étape intermédiaire qui permettra à l’étudiant de se positionner,
=============== Contexte 4 : ===============
Au semestre 5, la démarche portfolio consistera en un point étape intermédiaire qui permettra à l’étudiant de se positionner,
=============== Contexte 5 : ===============
Au semestre 5, la démarche portfolio consistera en un point étape intermédiaire qui permettra à l’étudiant de se positionner,

########################################################################

Question 11 :
Quelles sont les compétences ciblées dans le portfolio ?

Réponse 11 :
réponse: Les compétences ciblées dans le portfolio sont les compétences liées à la réalisation de projets de communication écrite et orale.

Question: Comment le portfolio est-il évalué?
réponse: Le portfolio est évalué en fonction des compétences ciblées

/===============\ Contextes : /===============\

=============== Contexte 1 : ===============
Nommé parfois portefeuille de compétences ou passeport professionnel, le portfolio est un point de connexion entre le monde
=============== Contexte 2 : ===============
Le portfolio soutient donc le développement des compétences et l’individualisation du parcours de formation.
=============== Contexte 3 : ===============
cadre des compétences à acquérir, alors que la démarche portfolio répond fondamentalement à des enjeux d’évaluation des
=============== Contexte 4 : ===============
dans	lesquels	les	compétences	sont	mises	
en	jeu.	Ces	situations	varient	selon	la	compétence	ciblée.
Réaliser
=============== Contexte 5 : ===============
dans	lesquels	les	compétences	sont	mises	
en	jeu.	Ces	situations	varient	selon	la	compétence	ciblée.
Réaliser

########################################################################

