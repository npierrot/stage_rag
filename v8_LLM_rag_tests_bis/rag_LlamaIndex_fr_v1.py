# ne pas oublier de lancer "ollama serve" dans un terminal avant de lancer le programme

from langchain_community.embeddings import HuggingFaceEmbeddings
from llama_index.core import VectorStoreIndex, Settings, ChatPromptTemplate
from llama_index.core.base.llms.types import ChatMessage, MessageRole
from llama_index.llms.ollama import Ollama
from llama_index.readers.file import PyMuPDFReader

file_path = "pdf/annexe-2-licence-professionnelle-bachelor-universitaire-de-technologie-informatique-29016.pdf"
reader = PyMuPDFReader()
documents = reader.load(file_path)

embedding_model = HuggingFaceEmbeddings(model_name="intfloat/multilingual-e5-large")

index = VectorStoreIndex.from_documents(
    documents,
    embed_model=embedding_model,
    show_progress=True,
)

llama = Ollama(
    model="mistral",
    request_timeout=600.0,
)

Settings.chunk_size = 100
Settings.chunk_overlap = 10
Settings.embed_model = embedding_model
Settings.num_output = 10

chat_text_qa_msgs = [
    ChatMessage(
        role=MessageRole.SYSTEM,
        content=(
            "Vous êtes un système expert de questions-réponses auquel le monde entier fait confiance.\n"
            "Répondez toujours à la requête en utilisant les informations contextuelles fournies"
            "et non vos connaissances préalables.\n"
            "Quelques règles à suivre :\n"
            "1. Ne faites jamais directement référence au contexte donné dans votre réponse.\n"
            "2. Évitez les déclarations telles que \"D'après le contexte, ...\" ou "
            "\"Les informations contextuelles ...\" ou toute autre déclaration de ce type."
        ),
    ),
    ChatMessage(
        role=MessageRole.USER,
        content=(
            "Les informations contextuelles sont ci-dessous.\n"
            "---------------------\n"
            "{context_str}\n"
            "---------------------\n"
            "Compte tenu des informations contextuelles et sans connaissances préalables, répondez à la requête.\n"
            "Requête :{query_str}\n"
            "Réponse : "
        ),
    ),
]
text_qa_template = ChatPromptTemplate(chat_text_qa_msgs)

query_engine = index.as_query_engine(llm=llama, text_qa_template=text_qa_template)

print(query_engine.query("Qu'est-ce que sont les SAE ?"))
