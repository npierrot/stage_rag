# https://huggingface.co/learn/cookbook/en/rag_llamaindex_librarian
# ne pas oublier de faire "ollama serve" dans un terminal

from langchain_community.embeddings import HuggingFaceEmbeddings
from llama_index.core import SimpleDirectoryReader, VectorStoreIndex
from llama_index.llms.ollama import Ollama

# on récupère les documents pour le rag
loader = SimpleDirectoryReader(
    input_dir="./Fichiers_a_lire",
    recursive=True,
    required_exts=[".txt"],
)

# on les charge
documents = loader.load_data()

# on définit le modèle d'embeddings à utiliser
embedding_model = HuggingFaceEmbeddings(model_name="BAAI/bge-small-en-v1.5")

# on index les documents avec les embeddings
index = VectorStoreIndex.from_documents(
    documents,
    embed_model=embedding_model,
)

# on charge le modèle à utiliser
llama = Ollama(
    model="llama2",
    request_timeout=40.0,
)

query_engine = index.as_query_engine(llm=llama)


print(query_engine.query("Quels sont les différents parcours qui composent la formation ?"))
