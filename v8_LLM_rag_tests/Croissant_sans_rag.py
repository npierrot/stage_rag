import torch
from transformers import AutoModelForCausalLM, AutoTokenizer

model_name = "croissantllm/CroissantLLMChat-v0.1"
tokenizer = AutoTokenizer.from_pretrained(model_name)
model = AutoModelForCausalLM.from_pretrained(model_name, torch_dtype=torch.float16, device_map="cuda")

i = 1

# On ouvre le fichier des questions
with open('Liste_Questions.txt', 'r', encoding='utf-8') as fichier:
    # on lit chaque ligne du fichier, qui correspondent à une question chacune
    for ligne in fichier:
        print("ligne " + str(i))
        # on génère la réponse de la question
        chat = [
            {"role": "user", "content": ligne},
        ]
        chat_input = tokenizer.apply_chat_template(chat, tokenize=False, add_generation_prompt=True)
        inputs = tokenizer(chat_input, return_tensors="pt", add_special_tokens=True).to(model.device)
        tokens = model.generate(**inputs, max_new_tokens=50, do_sample=True, top_p=0.95, top_k=60, temperature=0.3)
        reponse = tokenizer.decode(tokens[0])

        # on ouvre un nouveau fichier pour y écrire les questions, les réponses que le modèle a générées et les
        # passages du pdf qui ont servi à produire ces réponses
        with open('./Questions_Reponses_sans_rag/Questions_et_Reponses_v1.txt', 'a', encoding='utf-8') as fichier2:
            # on affiche la question et la réponse
            fichier2.write("Question " + str(i) + " :\n" + ligne + '\n'
                           "Réponse " + str(i) + " :\n" + reponse)

            fichier2.write('\n\n########################################################################\n\n')
            i += 1